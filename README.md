# Chat-server v0.2

Chat-server is a TCP client/server chat.

## Client

You must use the client to connect to the server on localhost port 5000 by default.
You have to enter your hostname to login.

## Server

The server listen on localhost, port 5000.
It's not a multithreaded server, it manage connections over a select.

