#!/usr/bin/python3
#==============================================================================
# title           : server.py
# description     : This is the server implementation of the chat-server.
# author          : Nicolas Gonzalez <nicolas@ngonzalez.fr>
# date            : 29.01.2017
# version         : 0.2
# usage           : ./server.py
# notes           :
# python_version  : 3.6.0
#==============================================================================

#!/usr/bin/python3

import socket, select

CONNECTION_LIST = []
USERNAME_DICT = {}
RECV_BUFFER = 4096
PORT = 5000

def broadcast_data (sock, message):
    for socket in CONNECTION_LIST:
        if socket != server_socket and socket != sock :
            try :
                socket.send(message)
            except :
                socket.close()
                CONNECTION_LIST.remove(socket)

if __name__ == "__main__":

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(("0.0.0.0", PORT))
    server_socket.listen(10)

    CONNECTION_LIST.append(server_socket)

    print("Chat server started on port " + str(PORT))

    while 1:
        read_sockets,write_sockets,error_sockets = select.select(CONNECTION_LIST,[],[])

        for sock in read_sockets:
            if sock == server_socket:
                sockfd, addr = server_socket.accept()
                CONNECTION_LIST.append(sockfd)

                try:
                    USERNAME_DICT[sockfd] = sockfd.recv(RECV_BUFFER).decode('utf-8')
                except:
                    print('Failed to get username')

                print("Client {}@{}:{} connected".format(USERNAME_DICT[sockfd], *addr))

                broadcast_data(sockfd, "\r[{}@{}:{}] entered room\n".format(USERNAME_DICT[sockfd], *addr).encode('utf-8'))
            else:
                try:
                    data = sock.recv(RECV_BUFFER).decode('utf-8')
                    if data:
                        message = '\r<{}> {}'.format(USERNAME_DICT[sock], data)
                        broadcast_data(sock, message.encode('utf-8'))
                except:
                    broadcast_data(sock, "\rClient {}@{}:{} is offline".format(USERNAME_DICT[sock], *addr).encode('utf-8'))
                    print("Client {}@{}:{} is offline".format(USERNAME_DICT[sock], *addr))
                    sock.close()
                    CONNECTION_LIST.remove(sock)
                    continue

    server_socket.close()
