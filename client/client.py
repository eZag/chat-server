#!/usr/bin/python3
#==============================================================================
# title           : client.py
# description     : This is the client implementation of the chat-server.
# author          : Nicolas Gonzalez <nicolas@ngonzalez.fr>
# date            : 29.01.2017
# version         : 0.2
# usage           : ./client.py <hostname> <port>
# notes           :
# python_version  : 3.6.0
#==============================================================================

import socket, select, string, sys, re

server_run = True

def prompt() :
    sys.stdout.write('<You> ')
    sys.stdout.flush()

def exec_internal_commands(cmd):
    global server_run
    if cmd[0] == "quit":
        server_run = False

if __name__ == "__main__":

    if(len(sys.argv) < 3) :
        print('Usage : ./client.py <hostname> <port>')
        sys.exit()

    host = sys.argv[1]
    port = int(sys.argv[2])
    username = input('Enter your username : ')

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(2)

    try :
        s.connect((host, port))
    except :
        print('Unable to connect')
        sys.exit()

    try:
        s.send(username.encode('utf-8'))
    except:
        print('Failed to login')
        sys.exit()

    print('Connected to remote host.')
    prompt()

    while server_run:
        socket_list = [sys.stdin, s]

        read_sockets, write_sockets, error_sockets = select.select(socket_list , [], [])

        for sock in read_sockets:
            if sock == s:
                data = sock.recv(4096)
                if not data :
                    print('\nDisconnected from chat server')
                    sys.exit()
                else :
                    sys.stdout.write(data.decode('utf-8'))
                    prompt()
            else :
                line = sys.stdin.readline()
                if re.match('\s*/[a-z|A-Z]', line):
                    line = re.sub('\s*/', '', line.rstrip())
                    line = re.sub(' +', ' ', line)
                    args = line.split(' ')
                    exec_internal_commands(args)
                else:
                    s.send(line.encode('utf-8'))
                if(server_run):
                    prompt()
    print('\nLogout from chat server')
    s.close()
